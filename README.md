# Bit2AWS Script - README #
---

### Overview ###

The **Bit2AWS** backup script (Bash) can clone and mirror all **Bitbucket** repositories of a specified user to a local computer or a host and creates a TAR file of each repository. Then it uploads all packed repositories to an **Amazon S3** bucket.

### Screenshots ###

![Bit2AWS Screen 1](development/readme/bit2aws1.png "Bit2AWS Screen 1")

![Bit2AWS Screen 2](development/readme/bit2aws2.png "Bit2AWS Screen 2")

### Setup ###

* Install and configure **GIT** on the host which should run the **Bit2AWS** script.
* Connect the **GIT** installation to the **Bitbucket** repositories (SSH).
* Install and configure the **s3cmd** command line tool.
* Check that the necessary software is installed: awk, curl, date, git, jq, python-magic, s3cmd, sendmail.
* Copy the backup script **bit2aws.sh** to your computer.
* Make the backup script executable: **chmod +x bit2aws.sh**.
* Start the backup script from the command prompt: **./bit2aws.sh**
* Follow the indications on the screen to check for errors.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **Bit2AWS** backup script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](http://opensource.org/licenses/MIT).
