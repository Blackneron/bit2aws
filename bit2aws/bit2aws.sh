#!/bin/bash

 #8888b.    d8b 888     .d8888b.         d8888 888       888  .d8888b.
 #88  "88b  Y8P 888    d88P  Y88b       d88888 888   o   888 d88P  Y88b
 #88  .88P      888           888      d88P888 888  d8b  888 Y88b.
 #888888K.  888 888888      .d88P     d88P 888 888 d888b 888  "Y888b.
 #88  "Y88b 888 888     .od888P"     d88P  888 888d88888b888     "Y88b.
 #88    888 888 888    d88P"        d88P   888 88888P Y88888       "888
 #88   d88P 888 Y88b.  888"        d8888888888 8888P   Y8888 Y88b  d88P
 #888888P"  888  "Y888 888888888  d88P     888 888P     Y888  "Y8888P"

########################################################################
#                                                                      #
#                            B I T 2 A W S                             #
#                                                                      #
#      B I T B U C K E T   T O   A M A Z O N   S 3   B A C K U P       #
#                                                                      #
#                      Copyright 2015 by PB-Soft                       #
#                                                                      #
#                           www.pb-soft.com                            #
#                                                                      #
#                             Version 1.5                              #
#                                                                      #
########################################################################
#                                                                      #
#  Please check that the following software packages are installed     #
#  and configured on your system:                                      #
#                                                                      #
#       awk, curl, date, git, jq, python-magic, s3cmd, sendmail        #
#                                                                      #
########################################################################
#                                                                      #
#  Please also check that you configure GIT after installing it:       #
#                                                                      #
#      git config --global user.name "Your Name"                       #
#      git config --global user.email "your.name@example.com"          #
#      git config --global push.default matching                       #
#                                                                      #
########################################################################
#                                                                      #
#  Please also check that you configure 's3cmd' after installing it:   #
#                                                                      #
#      s3cmd --configure  (follow the instructions)                    #
#                                                                      #
#  Please also check the following website for more information:       #
#                                                                      #
#      http://s3tools.org/s3cmd-howto                                  #
#                                                                      #
########################################################################
#                                                                      #
#  Script usage:                                                       #
#                                                                      #
#   $ ./bit2aws.sh        (script without any parameter)               #
#                                                                      #
#   => Starts the Bit2AWS backup for all Bitbucket repositories of     #
#      the specified user.                                             #
#                                                                      #
#                                                                      #
#   $ ./bit2aws.sh repo1  (script with a repository slug as parameter) #
#                                                                      #
#   => Starts the Bit2AWS backup for the repository 'repo1' of the     #
#      specified user.                                                 #
#                                                                      #
########################################################################


# ======================================================================
#            U S E R   C O N F I G U R A T I O N   B E G I N
# ======================================================================

# Please specify the Bitbucket username.
BITBUCKET_USERNAME='--your-username--'

# Please specify the Bitbucket password.
BITBUCKET_PASSWORD='--your-password--'

# Please specify the local backup path.
LOCAL_BACKUP_PATH=$HOME'/bitbucket'

# Please specify the Amazon S3 backup destination (bucket).
AWS_BACKUP_PATH="s3://bitbucket"

# Please specify the sender email address (host).
EMAIL_SENDER="admin@example.com"

# Please specify the receiver email address.
EMAIL_RECEIVER="john@example.com"

# Specify if the TAR files should be deleted.
DELETE_TAR_FILES=1

# Specify if the GIT cleanup is enabled.
GIT_CLEANUP=1

# Specify if the GIT integrity check is enabled.
GIT_INT_CHECK=0

# Please specify if emails should be sent.
SEND_EMAILS=1

# Please specify if the date/time should be added to the backup files.
ADD_BACKUP_DATE=0

# Please specify if the debug mode is enabled.
DEBUG_MODE=0

# If necessary please add additional paths to the environment PATH.
# This can be necessary if an executable like 'jq' or 'sendmail' can
# not be found by the system.
PATH=/usr/sbin:$PATH

# ======================================================================
#            U S E R   C O N F I G U R A T I O N   E N D
# ======================================================================

# Specify the script version.
SCRIPT_VERSION="1.5"

# Specify the internal version (minor changes).
INTERNAL_VERSION="16"

# Set the TERM environment variable to eliminate the error if the
# script is run from a cronjob.
export TERM=xterm

# Get the actual directory path of this script (script path).
script_path=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Add the script path to the environment variable PATH.
PATH=$script_path:$PATH

# Get the start time.
start_time=`date +%s`

# Get the repository name (parameter).
repository=$1

# Convert the repository name to lowercase.
repository="${repository,,}"


# ======================================================================
# Function to append text to the email content.
# ======================================================================
function fappend() {

  # Append the second function argument (email text) to the first
  # argument (temporary variable).
  echo "$2">>$1

} # Function to append text to the email content.


# ======================================================================
# Function to convert the filesize into a readable format.
# ======================================================================
function get_file_size() {

  # Get the input filesize.
  input_file_size=$1

  # Check if the filesize should be displayed in gigabytes.
  if [ "$input_file_size" -ge 1073741824 ]; then

    # Calculate the file size in gigabytes.
    output_file_size=$(awk "BEGIN {printf \"%.2f\",${input_file_size}/1073741824}")

    # Specify the size unit.
    output_size_unit="GB"

    # Check if the filesize should be displayed in megabytes.
  elif [ "$input_file_size" -ge 1048576 ]; then

    # Calculate the file size in megabytes.
    output_file_size=$(awk "BEGIN {printf \"%.2f\",${input_file_size}/1048576}")

    # Specify the size unit.
    output_size_unit="MB"

    # Check if the filesize should be displayed in kilobytes.
  elif [ "$input_file_size" -ge 1024 ]; then

    # Calculate the file size in kilobytes.
    output_file_size=$(awk "BEGIN {printf \"%.2f\",${input_file_size}/1024}")

    # Specify the size unit.
    output_size_unit="KB"

    # The filesize should be displayed in bytes.
  else

    # Set the filesize in bytes.
    output_file_size=$input_file_size

    # Specify the size unit.
    output_size_unit="B"

  fi # The filesize should be displayed in bytes.

} # Function to convert the filesize into a readable format.


# ======================================================================
# Display the start screen.
# ======================================================================

# Clear the screen.
clear

# Display the header information
echo "======================================================================"
echo
echo "            Bit2AWS - Repository Backup (Bitbucket to AWS)"
echo
echo "             Copyright 2015 by PB-Soft - www.pb-soft.com"
echo
echo "                            Version $SCRIPT_VERSION.$INTERNAL_VERSION"
echo
echo "======================================================================"

# Display an information message.
echo "Starting the Bit2AWS repository backup (Bitbucket to Amazon S3)..."


# ======================================================================
# Initialize the display options.
# ======================================================================

# Check if the debug mode is enabled.
if [ "$DEBUG_MODE" == 1 ]; then

  # Specify the display options for the curl command - Verbose.
  options_1='-v'

  # Specify the display options for the git remote command - Verbose.
  options_2='-v'

  # Specify the display options for the git clone command - Verbose.
  options_3='-v'

  # Specify the display options for the git gc command - Verbose.
  options_4=''

  # Specify the display options for the git fsck command - Verbose.
  options_5='--verbose'

  # Specify the display options for the s3cmd command - Verbose.
  options_6='-v -d'

  # Specify the options for the tar command - Verbose.
  options_7='-cvf'

  # The debug mode is not enabled.
else

  # Specify the display options for the curl command - Silent.
  options_1='-s'

  # Specify the display options for the git remote command - Silent.
  options_2=''

  # Specify the display options for the git clone command - Silent.
  options_3='-q'

  # Specify the display options for the git gc command - Silent.
  options_4='--quiet'

  # Specify the display options for the git fsck command - Silent.
  options_5=''

  # Specify the display options for the s3cmd command - Silent.
  # Versions > 1.5.0 have a 'quiet' option: '--quiet' os '-v'
  options_6='--quiet'

  # Specify the options for the tar command - Silent.
  options_7='-cf'

fi # The debug mode is not enabled.


# ======================================================================
# Limit GIT memory usage for smooth processing.
# ======================================================================

# Limit the memory usage for GIT.
# http://kidsreturn.org/2011/06/setup-git-server-gitolite/
git config --global core.packedGitWindowSize 16m
git config --global core.packedGitLimit 64m
git config --global pack.windowMemory 64m
git config --global pack.packSizeLimit 64m
git config --global pack.thread 1
git config --global pack.deltaCacheSize 1m


# ======================================================================
# Remove the trailing slashes from the two backup paths.
# ======================================================================

# Specify the regex pattern to detect trailing slashes.
regex_pattern='^.*[^/]'

# Remove trailing slashes from the local backup path.
[[ $LOCAL_BACKUP_PATH =~ $regex_pattern ]]
LOCAL_BACKUP_PATH=${BASH_REMATCH[0]}

# Remove trailing slashes from the AWS backup path.
[[ $AWS_BACKUP_PATH =~ $regex_pattern ]]
AWS_BACKUP_PATH=${BASH_REMATCH[0]}


# ======================================================================
# Create the local backup directory.
# ======================================================================

# Display an information message.
echo "Checking if the local backup directory already exist..."

# Check if the destination directory does not exist.
if [ ! -d "$LOCAL_BACKUP_PATH" ]; then

  # Display an information message.
  echo "Creating the local backup directory..."

  # Create the destination directory (with parent directories).
  mkdir -p $LOCAL_BACKUP_PATH

fi # Check if the destination directory does not exist.


# ======================================================================
# Get repository information from Bitbucket.
# ======================================================================

# Check if the repository parameter is empty.
if [ "$repository" == "" ]; then


  # ====================================================================
  # Receive information about all available repositories.
  # ====================================================================

  # Display an information message.
  echo "Receiving the information for ALL repositories from Bitbucket..."

  # Get the info (full_name/name) of all Bitbucket repositories.
  # https://confluence.atlassian.com/display/BITBUCKET/Version+2
  all_repos=`curl $options_1 -S --user $BITBUCKET_USERNAME:$BITBUCKET_PASSWORD https://api.bitbucket.org/2.0/repositories/$BITBUCKET_USERNAME\?pagelen\=100 | jq -r '.values[] | "\(.full_name),\(.name)"'`


  # ====================================================================
  # Check the repository information.
  # ====================================================================

  # Check if the repositories do not exist.
  if [ "$all_repos" == "" ]; then

    # Clear the screen.
    clear

    # Display an error message.
    echo "ERROR: No Bitbucket repositories were found!"
    echo "Exit the Bit2AWS repository backup..."
    echo

    # Exit the script.
    exit 1

    # The repositories exist.
  else

    # Display an information message.
    echo "Backup ALL Bitbucket repositories to the local computer..."

  fi # The repositories exist.

  # The repository parameter was specified.
else


  # ====================================================================
  # Receive information for only one repository.
  # ====================================================================

  # Display an information message.
  echo "Receiving the information for ONE repository from Bitbucket..."

  # Get the info (full_name/name) of only one Bitbucket repository.
  # https://confluence.atlassian.com/display/BITBUCKET/Version+2
  all_repos=`curl $options_1 -S --user $BITBUCKET_USERNAME:$BITBUCKET_PASSWORD https://api.bitbucket.org/2.0/repositories/$BITBUCKET_USERNAME/$repository | jq -r '"\(.full_name),\(.name)"'`


  # ====================================================================
  # Check the repository information.
  # ====================================================================

  # Check if the specified repository does not exist.
  if [ "$all_repos" == "null,null" ]; then

    # Clear the screen.
    clear

    # Display an error message.
    echo "ERROR: The specified Bitbucket repository '$repository' is not valid!"
    echo "Exit the Bit2AWS repository backup..."

    # Exit the script.
    exit 1

    # The specified repository does exist.
  else

    # Display an information message.
    echo "Backup only ONE Bitbucket repository to the local computer..."

  fi # The specified repository does exist.

fi # The repository parameter was specified.

# Insert a division line before the repository info is displayed.
echo "======================================================================"


# ======================================================================
# Initialize the variables for the loop.
# ======================================================================

# Change to the destination directory.
cd $LOCAL_BACKUP_PATH

# Initialize the email info string.
email_info=""

# Initialize the repository counter.
repository_counter=0

# Initialize the total filesize of all repositories.
total_file_size=0


# ======================================================================
# Start the repository backup.
# ======================================================================

# Loop through all the Bitbucket repositories.
for repo in $all_repos
do


  # ====================================================================
  # Get the name of the actual repository.
  # ====================================================================

  # Get the full name of the repository from the output.
  # cut -d [DELIMITER] -f [RANGE]
	repository_full_name=`echo $repo | cut -d ',' -f 1`

  # Get the name of the repository from the output.
  # cut -d [DELIMITER] -f [RANGE]
	repository_name=`echo $repo | cut -d ',' -f 2`

  # Specify the repository backup directory.
  repo_backup_dir="$LOCAL_BACKUP_PATH/$repository_name.git"

  # Increase the repository counter.
  repository_counter=$((repository_counter + 1))

  # Display the name of the actual processed repository.
  echo
  echo "======================================================================"
  echo "Processing the $repository_counter. repository: '$repository_name'..."
  echo "======================================================================"

  # Add the repository name to the email information output.
  email_info+="- $repository_name "

  # Display an information message.
  echo "Checking if the local repository '$repository_name' already exist..."

  # Check if the repository exist on the local computer.
  if [ -d "$repo_backup_dir" ]; then

    # ==================================================================
    # Update an existing local repository.
    # ==================================================================

    # Add the repository status to the email information output.
    email_info+="(Updated) - "

    # Display an information message.
    echo "Updating the existing '$repository_name' repository from Bitbucket..."

    # Change to the repository directory.
    cd "$repo_backup_dir"

    # Update the existing repository from Bitbucket.
    git remote $options_2 update 2>&1 | grep -v "Fetching origin"

    # The repository does not exist on the local computer.
  else


    # ==================================================================
    # Create a new local repository directory.
    # ==================================================================

    # Add the repository status to the email information output.
    email_info+="(New) - "

    # Display an information message.
    echo "Creating a new local '$repository_name' repository directory..."

    # Create a directory for the new Bitbucket repository (with parent
    # directories).
    mkdir -p "$repo_backup_dir"

    # Display an information message.
    echo "Cloning and mirroring the '$repository_name' repository from Bitbucket..."

    # Change to the repository directory.
    cd "$repo_backup_dir"

    # Clone the whole repository from Bitbucket.
    git clone $options_3 --mirror git@bitbucket.org:$repository_full_name.git $repo_backup_dir

  fi # The repository does not exist on the local computer.


  # ====================================================================
  # Cleanup the actual repository.
  # ====================================================================

  # Check if the GIT cleanup is enabled.
  if [ "$GIT_CLEANUP" == 1 ]; then

    # Display an information message.
    echo "Cleaning unnecessary files and optimize the local repository..."

    # Cleanup unnecessary files and optimize the local repository.
    git gc --auto $options_4

  fi # Check if the GIT cleanup is enabled.


  # ====================================================================
  # Check the integrity of the actual repository.
  # ====================================================================

  # Check if the GIT integrity check is enabled.
  if [ "$GIT_INT_CHECK" == 1 ]; then

    # Display an information message.
    echo "Checking the integrity of the '$repository_name' repository..."

    # Check the new repository.
    git fsck $options_5 --full

  fi # Check if the GIT integrity check is enabled.


  # ====================================================================
  # Pack the actual repository and create a TAR file.
  # ====================================================================

  # Display an information message.
  echo "Packing the '$repository_name' repository and create a TAR file..."

  # Specify the repository backup tar file.
  repo_backup_tar_file="$LOCAL_BACKUP_PATH/$repository_name.git.tar"

  # Pack the cloned repository (without compressing, because the file-
  # size will be increased during compression).
  tar $options_7 $repo_backup_tar_file $repo_backup_dir 2>&1 | grep -v  "Removing leading"


  # ====================================================================
  # Get the filesize of the repository package (TAR file).
  # ====================================================================

  # Get the filesize in bytes.
  file_size_b=$(stat -c%s "$repo_backup_tar_file")

  # Add the new filesize to the total filesize.
  total_file_size=$((total_file_size + file_size_b))


  # ====================================================================
  # Calculate the filesize of the actual repository.
  # ====================================================================

  # Convert the filesize into a readable format.
  get_file_size $file_size_b

  # Get the filesize from the function into a variable.
  file_size=$output_file_size

  # Get the size unit from the function into a variable.
  size_unit=$output_size_unit


  # ====================================================================
  # Display and store the filesize of the actual repository.
  # ====================================================================

  # Add the file size to the email information output.
  email_info+="TAR size: $file_size $size_unit"$'\n'

  # Display an information message.
  printf "Repository package for '$repository_name' created, filesize: %.2f $size_unit\n" $file_size


  # ====================================================================
  # Synchronize the actual repository to Amazon S3.
  # ====================================================================

  # Display an information message.
  echo "Synchronizing the '$repository_name' repository to the AWS bucket..."

  # Check if the backup date/time should be added to the backup file.
  if [ "$ADD_BACKUP_DATE" == 1 ]; then

    # Save the date and time into variables.
    now_date=$(date +"%Y-%m-%d")
    now_time=$(date +"%H-%M-%S")

    # Specify the filename for the AWS upload file.
    repo_backup_aws_file=$AWS_BACKUP_PATH"/"$now_date"_"$now_time"_"$repository_name".git.tar"

    # The backup date/time should not be added to the backup file.
  else

    # Specify the filename for the AWS upload file.
    repo_backup_aws_file=$AWS_BACKUP_PATH"/"$repository_name".git.tar"

  fi # The backup date/time should not be added to the backup file.

  # Synchronize the latest backup to Amazon S3.
  s3cmd sync $options_6 --no-delete-removed $repo_backup_tar_file $repo_backup_aws_file


  # ====================================================================
  # Remove the local backup package (uploaded AWS backup file).
  # ====================================================================

  # Check if the TAR backup files should be deleted.
  if [ "$DELETE_TAR_FILES" == 1 ]; then

    # Display an information message.
    echo "Deleting the local package file '$repository_name.git.tar'..."

    # Remove the uploaded backup file.
    rm -f $repo_backup_tar_file

  fi # Check if the TAR backup files should be deleted.

  # Display an information message.
  echo "The backup of the '$repository_name' repository has finished!"

  # Insert a division line before the next repository info is displayed.
  echo "======================================================================"

done # Loop through all the Bitbucket repositories.


# ======================================================================
# Calculate the total filesize of all repositories.
# ======================================================================

# Get the total filesize into a readable format.
get_file_size $total_file_size

# Get the total filesize from the function into a variable.
total_file_size=$output_file_size

# Get the total size unit from the function into a variable.
total_size_unit=$output_size_unit


# ======================================================================
# Calculate the processing time.
# ======================================================================

# Get the end time.
end_time=`date +%s`

# Get the processing time in seconds.
processing_time=$((end_time-start_time))


# ======================================================================
# Send an email confirmation message.
# ======================================================================

# Check if the sending of emails is enabled.
if [ "$SEND_EMAILS" == 1 ]; then

  # Display an information message.
  echo
  echo "======================================================================"
  echo "Sending a confirmation email to '$EMAIL_RECEIVER'..."

  # Specify the temporary file.
  temp=‘mktemp‘

  # Delete an existing temporary file.
  rm -f $temp

  # Specify the e-mail subject.
  now_date=$(date +%m.%d.%Y)
  now_time=$(date +%H:%M:%S)
  host_name=$(hostname)
  email_subject="Bit2AWS - Backup finished - $now_date / $now_time - $host_name"

  # Create the header to the email content.
  fappend $temp "From: $EMAIL_SENDER"
  fappend $temp "To: $EMAIL_RECEIVER"
  fappend $temp "Reply-To: $EMAIL_SENDER"
  fappend $temp "Subject: $email_subject"
  fappend $temp ""

  # Attach the body to the email content.
  fappend $temp "======================================================================"
  fappend $temp " Bit2AWS - Repository Backup (Bitbucket to AWS) - www.pb-soft.com"
  fappend $temp "======================================================================"
  fappend $temp ""
  fappend $temp "The Bit2AWS repository backup has finished!"
  fappend $temp ""
  fappend $temp "End time: $now_time"
  fappend $temp "End date: $now_date"
  fappend $temp "Script version: $SCRIPT_VERSION.$INTERNAL_VERSION"
  fappend $temp "Processing host: $host_name"
  fappend $temp ""
  fappend $temp "======================================================================"
  fappend $temp ""
  fappend $temp "Data path:"
  fappend $temp "1 - Backup source: bitbucket.org"
  fappend $temp "2 - Processing: $host_name"
  fappend $temp "3 - Backup target: $AWS_BACKUP_PATH"
  fappend $temp ""
  fappend $temp "======================================================================"
  fappend $temp ""
  fappend $temp "Backup of the following repositories:"
  fappend $temp ""
  fappend $temp "$email_info"
  fappend $temp "Number of repositories processed: $repository_counter"
  fappend $temp "Total size of all repositories: $total_file_size $total_size_unit"
  fappend $temp "Backup processing time: $processing_time seconds"
  fappend $temp ""
  fappend $temp "======================================================================"

  # The last two lines have to be empty!
  fappend $temp ""
  fappend $temp ""

  # Send the e-mail.
  cat $temp|sendmail -t

  # Remove the temporary file.
  rm -f $temp

fi # Check if the sending of emails is enabled.

# Display an information message.
echo
echo "Number of repositories processed: $repository_counter"
echo "Total size of all repositories: $total_file_size $total_size_unit"
echo "Backup processing time: $processing_time seconds"
echo
echo "The Bit2AWS backup process has finished successfully!"
echo "======================================================================"
echo
